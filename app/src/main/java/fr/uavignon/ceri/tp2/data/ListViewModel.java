package fr.uavignon.ceri.tp2.data;

import android.app.Application;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class ListViewModel {

    private BookRepository repository;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<List<Book>> selectBook;

    public ListViewModel (Application application) {
        super();
        repository = new BookRepository((application));
        allBooks = repository.getAllBooks();
        selectBook = repository.getSelectBook();

    }

    MutableLiveData<List<Book>> getSelectBook(){
        return selectBook;
    }

    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public void insertBook (Book book) {
        repository.insertBook(book);
    }

    public void findBook (String name) {
        repository.findBook(name);
    }

    public void deleteBook(String name) {
        repository.deleteBook(name);
    }
}
