package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private MutableLiveData<List<Book>> selectBook = new MutableLiveData<>();
    public LiveData<List<Book>> allBooks;

   // public Book getBooks;

    public BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
        //getBooks = bookDao.getBook();

    }

    /**public Book getBook(){
        return getBooks;
    }**/

    public LiveData<List<Book>> getAllBooks(){
        return allBooks;
    }

    public MutableLiveData<List<Book>> getSelectBook() {
        return selectBook;
    }

    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }

    public void deleteBook(String name) {
        databaseWriteExecutor.execute( () -> {
            bookDao.deleteBook(name);
        });
    }

    public void findBook(String name){

        Future<List<Book>> fbooks = databaseWriteExecutor.submit(()-> {
            return bookDao.findBook(name);
        });

        try {
            selectBook.setValue(fbooks.get());
        }catch (ExecutionException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
